# Inware translations

## How to translate
There are 2 files to translate:
1. [strings.xml](https://gitlab.com/EvoWizz/inware-tanslations/blob/master/english_original/strings.xml):

	Most strings look like this:
	```xml
	<string name="system_tab">System</string>
	```
	In this case, you should **not** translate `system_tab`, but only `System`.

	Some other strings look like this:
	```xml
	<string name="core_number">Core <xliff:g id="number_of_core" example="3">%d</xliff:g></string>
	```
	Here, you should **only** tanslate `Core`. Also, do not forget whitespaces.

2. [faq.json](https://gitlab.com/EvoWizz/inware-tanslations/blob/master/english_original/faq.json):
	
    Feel free to use an online tool like [JSON Editor Online](https://jsoneditoronline.org/) to help you with this file.
    
    There are 2 kinds of objects in this file:
    
    1. **Categories**:
    	```json
        {
        "type": 1,
        "title": "Data",
        "key": "dataCategory"
        }
        ```
        For categories, you should only translate the `title`.
        
	2. **Questions**:
		```json
        {
        "type": 2,
        "title": "Why is the Read Phone State permission needed?",
        "summary": "It is used to display your device’s IMEI number.",
        "key": "dataInfo1"
        }
        ```
        For questions, you should only translate the `title` and the `summary`.
        
        
## How to add your translations

To add your translations in the repository, you need to create a merge request (See: [How to create a merge request?](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)) with the translated files inside their respective directory (eg. hindi, spanish) or send me a mail with the translated files.


## License
```
Copyright 2018 Dylan (EvoWizz)

Licensed to the Apache Software Foundation (ASF) under one or more contributor
license agreements. See the NOTICE file distributed with this work for
additional information regarding copyright ownership. The ASF licenses this
file to you under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
```